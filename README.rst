===============
python-minibrew
===============

Introduction
============

python-minibrew is a Python utility to fetch data from the Minibrew API and 
send it to Librato. Since the Minibrew setup is installed at my work place 
where we use Librato to gather metrics it was the most practical choice to use 
that as the backend for the metrics.

Features
========

- Fetch data from basic endpoints (time remaining, current and target 
  temperature)
- Send data to Librato


Installation
============

Make sure you have the requests and librato-metrics Python 3 modules 
installed. Then clone the repository.

Usage
=====

Create a ``.creds`` file in the ``python-minibrew`` directory with the 
following lines:

::

  librato_username=<LIBRATO_USERNAME>
  librato_token=<LIBRATO_TOKEN>
  minibrew_username=<MINIBREW_USERNAME>
  minibrew_password=<MINIBREW_PASSWORD>

TODO
====

- Expose more endpoints
- Add other backends

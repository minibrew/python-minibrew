#!/usr/bin/env python3

import requests
import librato
import os
import re

credentials = {}
credentials["file"] = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                      ".creds"))


def get_credentials():
    with open(credentials["file"]) as f:
        lines = f.readlines()
        for line in lines:
            split_line = line.strip().split('=')
            credentials[split_line[0]] = split_line[1]


def get_token():
    portal_url = "https://pro.minibrew.io"
    portal_response = requests.get(portal_url)
    portal_js = re.search('src="/(.*.js)"', portal_response.text).group(1)

    token_url = f"{portal_url}/{portal_js}"
    token_response = requests.get(token_url)
    token = re.search('TOKEN [a-z,0-9]+', token_response.text).group(0)

    return token


def get_bearer_token(token):
    auth_url = "https://api.minibrew.io/v2/token/"
    auth_headers = {"host": "api.minibrew.io",
                    "authorization": token}
    auth_payload = {"email": credentials["minibrew_username"],
                    "password": credentials["minibrew_password"]}
    auth_response = requests.post(auth_url,
                                  data=auth_payload,
                                  headers=auth_headers)
    auth_json = auth_response.json()
    bearer_token = f"Bearer {auth_json['token']}"

    return bearer_token


def get_data(bearer_token):
    api_url = "https://api.minibrew.io/v1"
    api_headers = {"host": "api.minibrew.io",
                   "authorization": bearer_token,
                   "client": "Breweryportal"}

    overview_url = f"{api_url}/breweryoverview/"
    overview_stages = ["Primary", "Clarification"]
    overview_response = requests.get(overview_url, headers=api_headers)
    overview_data = overview_response.json()

    devices_url = f"{api_url}/devices/"
    devices_response = requests.get(devices_url, headers=api_headers)
    devices_data = devices_response.json()

    for key in overview_data:
        data_list = overview_data[key]
        if len(data_list) != 0:
            beer_tmp_data = data_list[0]
        if beer_tmp_data["stage"] in overview_stages:
            beer_data = beer_tmp_data
            beer_data["beer_name"] = beer_data["beer_name"].lower().replace(
                " ", "_")

    sessions_url = f'{api_url}/sessions/{beer_data["session_id"]}/'
    sessions_response = requests.get(sessions_url, headers=api_headers)
    sessions_data = sessions_response.json()

    return beer_data


def librato_monitoring(beer_data):
    beer_name = beer_data["beer_name"]
    beer_time_remaining = int(beer_data["status_time"] / 86400)
    librato_api = librato.connect(credentials["librato_username"],
                                  credentials["librato_token"])
    librato_queue = librato_api.new_queue()
    librato_queue.add("minibrew.target_temp",
                      beer_data["target_temp"],
                      tags={'device': 'minibrew', 'beer_name': beer_name})
    librato_queue.add("minibrew.current_temp",
                      beer_data["current_temp"],
                      tags={'device': 'minibrew', 'beer_name': beer_name})
    librato_queue.add("minibrew.time_remaining",
                      beer_time_remaining,
                      tags={'device': 'minibrew', 'beer_name': beer_name})
    librato_queue.submit()


def main():
    get_credentials()
    token = get_token()
    bearer_token = get_bearer_token(token)
    beer_data = get_data(bearer_token)
    librato_monitoring(beer_data)


if __name__ == "__main__":
    main()
